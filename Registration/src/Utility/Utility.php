<?php

namespace App\Utility;
class Utility {
    
    static public function d($param=FALSE){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
        
    }
    
    static public function dd($param=FALSE){
        self::d($param);
        die();
    }
    
    static public function redirect($url){
        header("Location:".$url);
    }
    
    static public function message($message = NULL){
        if(is_null($message)){
           $_message = $_SESSION['message'];
           $_SESSION['message'] = NULL;
           return $_message;
        }
 else {
     $_SESSION['message'] = $message;
 }
    }
    
    
}

?>

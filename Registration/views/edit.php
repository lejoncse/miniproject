<?php
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Registration'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');
use App\Person\User;
use App\Utility\Utility;

$obj = new User();

$user = $obj->show($_GET['id']);


?>
<!DOCTYPE html>
<html>
    <head>
        <title>Add New User</title>
        <link href="../resource/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                   <div class="page-header">
  <h1>Edit Info</h1>
</div> 
                    <div class="col-md-5">
  <ul class="nav nav-pills">
    
          <li role="presentation" class="alert-info"><a href="index.php">Show List</a></li>
          

</ul>   
                    <form action="update.php" method="post" enctype="multipart/form-data">
  <div class="form-group">
    
    <input type="hidden" class="form-control" id="Id" name="id" placeholder="Enter Full Name" value="<?php echo $user['id'] ?>">
  </div>                      
  <div class="form-group">
    <label for="Name">Name</label>
    <input type="text" class="form-control" id="Name" name="name" placeholder="Enter Full Name" value="<?php echo $user['name'] ?>">
  </div>
  <div class="form-group">
    <label for="email">Email</label>
    <input type="email" class="form-control" id="email" name="email" placeholder="Enter valid email" value="<?php echo $user['email'] ?>">
  </div>
  <div class="form-group">
    <label for="Phone">Phone Number</label>
    <input type="text" class="form-control" id="Phone" name="phone" placeholder="Enter Phone Number" value="<?php echo $user['phone'] ?>">
  </div>
<?php $gender= $user['gender'] ;
 if($gender==='Male'){
     $male="checked";
 } 
  if($gender==='Female'){
     $female="checked";
 }  
 ?>
  <div class="form-group">
      <label>Select Gender</label>
    <div class="radio">
        <label><input type="radio" name="gender"  value="Male" <?php echo $male?> >Male</label>
</div>
<div class="radio">
    <label><input type="radio" name="gender" value="Female" <?php echo $female?>>Female</label>
</div>
                       
  </div>
<?php 
$city= $user['city'] ;
 if($city==='Dhaka'){
     $dhaka="selected";
 } 
  if($city==='Chittagong'){
     $chittagong="selected";
 } 
 if($city==='Sylhet'){
     $sylhet="selected";
 } 
 if($city==='Rajshahi'){
     $rajshahi="selected";
 } 
 if($city==='Khulna'){
     $khulna="selected";
 } 
 if($city==='Barisal'){
     $barisal="selected";
 } 
 if($city==='Rangpur'){
     $rangpur="selected";
 } 
 if($city==='Mymensingh'){
     $mymensingh="selected";
 } 
 ?>
 <div class="form-group">
 <label for="city">Select Divisional City:</label>
 <select class="form-control" id="city" name="city">
    <option <?php echo $dhaka;?>>Dhaka</option>
    <option <?php echo $chittagong;?>>Chittagong</option>
    <option <?php echo $sylhet;?>>Sylhet</option>
    <option <?php echo $rajshahi;?>>Rajshahi</option>
    <option <?php echo $khulna;?>>Khulna</option>
    <option <?php echo $barisalarisal;?>>Barisal</option>
    <option <?php echo $rangpur;?>>Rangpur</option>
    <option <?php echo $mymensingh;?>>Mymensingh</option>
  </select>
                        </div>
  <div class="form-group">
    <label for="picture">Profile Picture</label>
    <input type="file" id="picture" name="picture">
<?php echo "<img src='images/$user[picture]' style='height=100px; width:100px;'>";?><br>    
  </div>
  <div class="checkbox">
    <label>
        <input type="checkbox" required=""> Accept Terms & Conditions
    </label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
                    </div>
                </div>
                
            </div>
            
        </div>
    </body>
</html>

<?php 
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Registration'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');

use App\Person\User;
use App\Utility\Utility;
$user = new User();
$users = $user->index();

?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registration</title>
        <link href="../resource/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/>
    </head>
    <body>
        <?php
        // put your code here
        ?>
        <div class="container">
            <div class="container-fluid">
                <div class=" row">
                    <div class="col-md-8">
                    <ul class="nav nav-pills">
                        <li role="presentation" class="active"><a href="create.html">Add New User</a></li>
                        <li role="presentation" class="alert-info" style="float: right"><a href="contact_list.php" class="glyphicon glyphicon-download"> Download list as XL</a></li>
      

</ul>
                     <div id="message" class="alert-success">
        <?php echo Utility::message();?>
        </div>
                    <table class="table table-bordered">
                        <tr>
                            <th>Sl.</th>
                            <th>Name</th>
                            <th>Action</th>
                        </tr>
                         <?php
             $no = 1;
             foreach ($users as $user){
             ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $user['name'] ?></td>
                            <td><div class="btn-group" role="group" aria-label="...">
                                    <a type="button" href="view.php?id=<?php echo $user['id'] ?>" class="btn btn-success glyphicon glyphicon-folder-open"> View Full Profile</a>
                                    <a type="button" href="edit.php?id=<?php echo $user['id'] ?>" class="btn btn-primary glyphicon glyphicon-pencil"> Edit</a>
                                    <a type="button" href="delete.php?id=<?php echo $user['id'] ?>" class="delete btn btn-danger glyphicon glyphicon-trash"> Delete</a>
</div></td>
                        </tr>
                        <?php 
             $no++;
             }
             
             ?>
                    </table> 
                </div> 
                </div>
            </div>
                
            </div>
            
        </div>
       <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
        <script>
         $('.delete').bind('click',function (){
             var DeleteItem = confirm("Are You sure, you want to delete?");
             if(!DeleteItem){
                 return false;
             }
         });
         
         $('#message').hide(4000);
        
        
        
        
        
        
        </script> 
    </body>
</html>

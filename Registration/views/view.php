<?php 
include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Registration'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'startup.php');

use App\Person\User;
use App\Utility\Utility;
$obj = new User();
$user = $obj->show($_GET['id']);

?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../resource/bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet"/>
    
    </head>
    <body>
        <div class="container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-5">
                      <div class="page-header">
  <h1>Full Profile</h1>
</div>   
                        <ul class="nav nav-pills">
    
          <li role="presentation" class="alert-info"><a href="index.php">Show List</a></li>
          

</ul> 
                       
                        <div class="img-thumbnail">
 <?php echo "<img src='images/$user[picture]' style='height=100px; width:100px;'>";?><br>                           
                        </div>
                        <div class="info">
                             
                            <label>Name:<?php echo $user['name']; ?></label> <br>
                            <label>Email:<?php echo $user['email']; ?></label><br>
                            <label>Phone:<?php echo $user['phone']; ?></label><br>
                            <label>Gender:<?php echo $user['gender']; ?></label><br>
                            <label>City:<?php echo $user['city']; ?></label><br>
                           
                        </div>
                    </div> 
                </div>  
                </div>
            </div>  
            
    </body>
</html>

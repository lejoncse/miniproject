-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 27, 2016 at 10:05 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `result`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_std`
--

CREATE TABLE IF NOT EXISTS `tbl_std` (
`id` int(11) NOT NULL,
  `seip_id` varchar(6) NOT NULL,
  `course_name` varchar(255) NOT NULL,
  `course_duration` tinyint(4) NOT NULL,
  `reg_date` date NOT NULL,
  `std_name` varchar(255) NOT NULL,
  `std_email` varchar(255) NOT NULL,
  `std_mobile` varchar(11) NOT NULL,
  `obtained_mark` varchar(255) NOT NULL,
  `batch` varchar(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_std`
--

INSERT INTO `tbl_std` (`id`, `seip_id`, `course_name`, `course_duration`, `reg_date`, `std_name`, `std_email`, `std_mobile`, `obtained_mark`, `batch`) VALUES
(1, '107219', 'Web App. Dev. PHP', 3, '2015-11-07', 'Md. Aminul Islam', 'amin@email.com', '01521206706', 'a:5:{s:10:"attendance";i:50;s:8:"lab_exam";i:55;s:14:"atomic_project";i:65;s:13:"final_project";i:65;s:17:"presentation_viva";i:55;}', 'B10'),
(5, '123123', '', 0, '0000-00-00', 'Arif Vai', '', '', 'a:3:{i:0;s:2:"40";i:1;s:2:"50";i:2;s:2:"60";}', ''),
(6, '110455', '', 0, '0000-00-00', 'Leon Vai', '', '', 'a:3:{i:0;s:2:"80";i:1;s:2:"90";i:2;s:3:"100";}', ''),
(7, '420420', '', 0, '0000-00-00', 'piyash Vai', '', '', 'a:5:{i:0;s:3:"100";i:1;s:2:"99";i:2;s:0:"";i:3;s:2:"33";i:4;s:1:"1";}', ''),
(8, '987654', '', 0, '0000-00-00', 'Someone Student', '', '', 'a:5:{i:0;s:2:"50";i:1;s:2:"60";i:2;s:2:"70";i:3;s:2:"80";i:4;s:2:"90";}', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_std`
--
ALTER TABLE `tbl_std`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_std`
--
ALTER TABLE `tbl_std`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

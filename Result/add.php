<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Add Record</title>
		<link rel="stylesheet" type="text/css" href="./App/Resource/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="./App/Resource/bootstrap/css/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="./App/Resource/font_awesome/css/font_awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="./App/Resource/custom/css/style.css" />
	</head>
	
	
	<body>
		
		<h1 class="text-center">Add Students Records</h1>
		
		<div class="row-fluid">
			<div class="col-md-4 col-md-offset-4">
				<form class="form-horizontal" action="App/View/store.php" method="POST">
				
					<fieldset>
						<legend>Student's Information</legend>
						
						<div class="form-group">
							<label for="std_name" class="col-sm-4 control-label">Students Name</label>
							<div class="col-sm-8">
								<input type="text" name="std_name" class="form-control" id="std_name" placeholder="Students Name">
							</div>
						</div>
							
						<div class="form-group">
							<label for="seip_id" class="col-sm-4 control-label">SEIP ID</label>
							<div class="col-sm-8">
								<input type="text" name="seip_id" class="form-control" id="seip_id" placeholder="123456">
							</div>
						</div>
						
					</fieldset>
					
					<fieldset>
						<legend>Marks</legend>
						
							
						<div class="form-group">
							<label for="lab_exam_1" class="col-sm-4 control-label">Lab Exam 1</label>
							<div class="col-sm-8">
								<input type="text" name="marks[]" class="form-control" id="lab_exam_1" placeholder="marks out of 100">
							</div>
						</div>
							
						<div class="form-group">
							<label for="lab_exam_2" class="col-sm-4 control-label">Lab Exam 2</label>
							<div class="col-sm-8">
								<input type="text" name="marks[]" class="form-control" id="lab_exam_2" placeholder="marks out of 100">
							</div>
						</div>
							
						<div class="form-group">
							<label for="lab_exam_3" class="col-sm-4 control-label">Lab Exam 3</label>
							<div class="col-sm-8">
								<input type="text" name="marks[]" class="form-control" id="lab_exam_3" placeholder="marks out of 100">
							</div>
						</div>
							
						<div class="form-group">
							<label for="lab_exam_4" class="col-sm-4 control-label">Lab Exam 4</label>
							<div class="col-sm-8">
								<input type="text" name="marks[]" class="form-control" id="lab_exam_4" placeholder="marks out of 100">
							</div>
						</div>
							
						<div class="form-group">
							<label for="lab_exam_5" class="col-sm-4 control-label">Lab Exam 5</label>
							<div class="col-sm-8">
								<input type="text" name="marks[]" class="form-control" id="lab_exam_4" placeholder="marks out of 100">
							</div>
						</div>
						
						
					</fieldset>
					
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-ok"></i> Submit</button>
							<button type="reset" class="btn btn-default"><i class="fa fa-refresh fa-spin"></i> Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<script type="text/JavaScript" src="./App/Resource/jquery/jquery.min.js"></script>
		<script type="text/JavaScript" src="./App/Resource/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/JavaScript" src="./App/Resource/custom/js/script.js"></script>
	</body>
	
</html>
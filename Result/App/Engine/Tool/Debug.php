<?php 

namespace App\Engine\Tool;

class Debug
{
	static public function d($x=false){
		echo "<pre>";
		var_dump($x);
		echo "</pre>";
	}
	
	static public function dd($x=false){
		self::d($x);
		die("Var Dumped & Execution Died!");
	}
	
	static public function pd($x=false){
		echo "<pre>";
		print_r($x);
		echo "</pre>";
	}
	
	static public function pdd($x=false){
		self::pd($param);
		die("Printed Out & Execution Died!");
	}
	
}

?>
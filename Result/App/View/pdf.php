<?php 
		
function __autoload($className){
	$file = './../../'.str_replace('\\','/',$className).'.php';
	//echo $file;
	require_once($file);
}

use \App\Engine\Code\File;
use \App\Engine\Tool\Debug;

if($_SERVER['REQUEST_METHOD']==='GET'){
	if(count($_GET)===1 && array_key_exists('seip_id',$_GET) && !empty($_GET['seip_id']) && strlen($_GET['seip_id'])===6 ){
		$seip_id=$_GET['seip_id']; 
		$obj = new File;
		$result=$obj->get($seip_id);
		//Debug::dd($result);
	}else{
		die('<h1 style="color:red">ERROR/INVALID ID !!!');
	}
}else{
	die('<h1 style="color:red">ERROR! Please Try Again Later!!!</h1>');
}

?><?php
require_once("./../Library/fpdf/fpdf.php");

$pdf = new FPDF();

$pdf->AddPage();
//$pdf->Image('bitm.jpg',12,13,'jpg');
$pdf->Image('gov.png',70,70,75,0,'PNG');

$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,20,'BASIS INSTITUTION OF TECHNOLOGY & MANAGEMENT',1,1,'C');
$pdf->SetFont('Arial','BU',12);
$pdf->Cell(0,20,'SEIP RESULT',0,1,'C');

$pdf->SetFont('Arial','B',10);
$pdf->Cell(35,10,'Students Name',0,0,'L');
$pdf->Cell(5,10,':',0,0,'C');
$pdf->SetFont('Arial','BI',10);
$pdf->Cell(90,10,$result->std_name,0,0);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(35,10,'SEIP ID',0,0,'R');
$pdf->Cell(5,10,':',0,0,'C');
$pdf->SetFont('Arial','BI',10);
$pdf->Cell(30,10,$result->seip_id,0,1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(35,10,'SEIP Course Name',0,0,'L');
$pdf->Cell(5,10,':',0,0,'C');
$pdf->SetFont('Arial','BI',10);
$pdf->Cell(90,10,$result->course_name,0,0);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(35,10,'Course Duration',0,0,'R');
$pdf->Cell(5,10,':',0,0,'C');
$pdf->SetFont('Arial','BI',10);
$pdf->Cell(30,10,$result->course_duration.' months',0,1);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(35,10,'Registration Email',0,0,'L');
$pdf->Cell(5,10,':',0,0,'C');
$pdf->SetFont('Arial','BI',10);
$pdf->Cell(50,10,$result->std_email,0,0);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(35,10,'Date of Registration',0,0,'R');
$pdf->Cell(5,10,':',0,0,'C');
$pdf->SetFont('Arial','BI',10);
$pdf->Cell(20,10,$result->reg_date,0,0);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(20,10,'Batch',0,0,'R');
$pdf->Cell(5,10,':',0,0,'C');
$pdf->SetFont('Arial','BI',10);
$pdf->Cell(10,10,$result->batch,0,1);

$pdf->Ln(10);

$pdf->SetFont('Arial','B',10);
$pdf->Cell(10,10,'#',1,0,'C');
$pdf->Cell(60,10,'SUBJECTS',1,0,'C');
$pdf->Cell(60,10,'50',1,0,'C');
$pdf->Cell(60,10,'OUT OF',1,1,'C');

$pdf->SetFont('Arial','',10);

$astring = $result->obtained_mark;
$array = unserialize($astring);
foreach($array AS $subj=>$mark){

	$pdf->Cell(10,10,'01. ',1,0,'R');
	$pdf->Cell(60,10,$subj,1,0,'C');
	$pdf->Cell(60,10,$mark,1,0,'C');
	$pdf->Cell(60,10,'100',1,1,'C');
		
}

$pdf->SetFont('Arial','B',10);
$pdf->Cell(70,10,'Total = ',1,0,'R');
$total=array_sum($array);
$pdf->Cell(60,10,$total,1,0,'C');
$pdf->Cell(60,10,'500',1,1,'C');
$avg = array_sum($array)/5;
$avg = round($avg,2);
$pdf->Cell(70,10,'Avg. (%)',1,0,'R');
$pdf->Cell(60,10,$avg,1,0,'C');
$pdf->Cell(60,10,'100',1,1,'C');


$pdf->Output();
?>

<?php
/*
require_once("./../Library/fpdf/fpdf.php");

$pdf = new FPDF('P','mm','A4');
$pdf->AddPage();
$pdf->image('bitm.jpg',12,13);
$pdf->Image('gov.png',50,100,100,100);

$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,20,'SEIP Result',1,1,'C');

$pdf->SetFont('Arial','B',16);
$pdf->Cell(0,10,'SEIP Result',0,1,'C');
$pdf->Output();
*/
?>
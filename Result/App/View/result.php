<?php 
		
function __autoload($className){
	$file = './../../'.str_replace('\\','/',$className).'.php';
	//echo $file;
	require_once($file);
}

use \App\Engine\Code\File;
use \App\Engine\Tool\Debug;

if($_SERVER['REQUEST_METHOD']==='POST'){
	if(count($_POST)===1 && array_key_exists('seip_id',$_POST) && !empty($_POST['seip_id']) && strlen($_POST['seip_id'])===6 ){
		$seip_id=$_POST['seip_id']; 
		$obj = new File;
		$result=$obj->get($seip_id);
		//Debug::pd($result);
	}else{
		die('<h1 style="color:red">ERROR/INVALID ID !!!');
	}
}else{
	die('<h1 style="color:red">ERROR! Please Try Again Later!!!</h1>');
}

?>
<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Students' Result</title>
		<link rel="stylesheet" type="text/css" href="./../Resource/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="./../Resource/bootstrap/css/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="./../Resource/font_awesome/css/font_awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="./../Resource/custom/css/style.css" />
	</head>
	<body>
		<div class="jumbotron">
			<div class="container text-center">
				<h1><span class="glyphicon glyphicon-education fa-lg"></span>&nbsp; SEIP Result</h1>
				<p>BASIS INSTITUION OF TECHNOLOGY & MANAGEMENT</p>
				<p>
					<a class="btn btn-danger btn-lg" href="pdf.php?seip_id=<?php echo $result->seip_id;?>" role="button"><i class="fa fa-file-pdf-o fa-lg"></i> Download Result as PDF</a>
					<a class="btn btn-success btn-lg" href="excel.php" role="button"><i class="fa fa-file-excel-o fa-lg"></i> Download Result as EXCEL</a>
				</p>
			</div>
		</div>

		<div class="container">
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-user fa-lg"></i>&nbsp; Students' Information
						</h3>
					</div>
					<div class="panel-body">
						<table class="table">
							<thead>
							
							</thead>
							<tbody>
								<tr>
									<td>Student's Name</td>
									<td><?php echo $result->std_name;?></td>
								</tr>
								<tr>
									<td>Course Name</td>
									<td><?php echo $result->course_name;?></td>
								</tr>
								<tr>
									<td>Course Duration</td>
									<td><?php echo $result->course_duration;?> months</td>
								</tr>
								<tr>
									<td>Batch Name</td>
									<td><?php echo $result->batch;?></td>
								</tr>
								<tr>
									<td>SEIP ID</td>
									<td><?php echo $result->seip_id;?></td>
								</tr>
								<tr>
									<td>Date of Registration</td>
									<td><?php echo $result->reg_date;?></td>
								</tr>
								<tr>
									<td>Email</td>
									<td><?php echo $result->std_email;?></td>
								</tr>
								<tr>
									<td>Contact</td>
									<td><?php echo $result->std_mobile;?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="glyphicon glyphicon-education fa-lg"></i>&nbsp; Result
						</h3>
					</div>
					<div class="panel-body">
						<table class="table">
							<thead>
								<tr>
									<th>Subjects</td>
									<th>Obtained Marks</td>
									<th>Out of</td>
								</tr>
							</thead>
							<tbody>
							
							<?php 
							$astring = $result->obtained_mark;
							$array = unserialize($astring);
							foreach($array AS $subj=>$mark){
							
							?>
							
								<tr>
									<td><?php echo $subj?></td>
									<td class="text-center"><?php echo $mark?></td>
									<td class="text-center">100</td>
								</tr>
							<?php 
							}
							?>								
							</tbody>
							<tfoot class="">
								<tr>
									<td class="text-right"><b>Total =</b></td>
									<td class="text-center"><b><?php echo array_sum($array)?></b></td>
									<td class="text-center"><b>500</b></td>
								</tr>
							</tfoot>
						</table>
						<p class="text-right">
							<a class="" href="JavaScript:window.print()" role="button"><i class="fa fa-print fa-lg"></i> Print</a> &nbsp;
							<a class="" href="pdf.php" role="button"><i class="fa fa-file-pdf-o fa-lg"></i> PDF</a> &nbsp;
							<a class="" href="excel.php" role="button"><i class="fa fa-file-excel-o fa-lg"></i> Excel</a> &nbsp;
						</p>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-12">
				<footer class="">
					<p class="text-center">
						Designed & Developed By <i class="fa fa-code fa-lg"> The Code Squad</i>
					</p>
				</footer>
			</div>
		</div>
		
		
		
		
		<script type="text/JavaScript" src="./../Resource/jquery/jquery.min.js"></script>
		<script type="text/JavaScript" src="./../Resource/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/JavaScript" src="./../Resource/custom/js/script.js"></script>
	</body>
</html>
<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Students' Result</title>
		<link rel="stylesheet" type="text/css" href="./../Resource/bootstrap/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="./../Resource/bootstrap/css/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="./../Resource/font_awesome/css/font_awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="./../Resource/custom/css/style.css" />
	</head>
	<body>
		<div class="jumbotron">
			<div class="container text-center">
				<h1><span class="glyphicon glyphicon-education fa-lg"></span>&nbsp; SEIP Result</h1>
				<p>BASIS INSTITUION OF TECHNOLOGY & MANAGEMENT</p>
				<p>
					<a class="btn btn-primary btn-lg" href="#" role="button"><i class="fa fa-spinner fa-spin fa-lg"></i> Learn About BITM...</a>
				</p>
			</div>
		</div>

		<div class="row-fluid">
			<div class="col-md-4 col-md-offset-4">
				<form class="form-horizontal" action="result.php" method="POST">
					<div class="form-group">
						<label for="course_name" class="col-sm-4 control-label">Course</label>
						<div class="col-sm-8">
							<select class="form-control" id="course_name">
								<option>--select--</option>
								<option value="php">Web App. Dev.(PHP)</option>
								<option value="dot_net">Web App. Dev.(DOT.NET)</option>
								<option value="web_design">Web Design</option>
								<option value="graphics_design">Graphics Design</option>
								<option value="android_app">Android App. Dev.</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="batch_name" class="col-sm-4 control-label">Batch</label>
						<div class="col-sm-8">
							<select  class="form-control" id="batch_name">
								<option>--select--</option>
								<option value="B10" >Batch 10</option>
								<option value="B11" >Batch 11</option>
								<option value="B12" disabled>Batch 12</option>
								<option value="B13" disabled>Batch 13</option>
								<option value="B14" disabled>Batch 14</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="seip_id" class="col-sm-4 control-label">SEIP ID</label>
						<div class="col-sm-8">
							<input type="text" name="seip_id" class="form-control" id="seip_id" placeholder="123456">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-4 col-sm-8">
							<button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-ok"></i> Get Result</button>
							<button type="reset" class="btn btn-default"><i class="fa fa-refresh fa-spin"></i> Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="col-md-12">
				<footer>
					<p class="text-center">
						Designed & Developed By <i class="fa fa-code fa-lg"> The Code Squad</i>
					</p>
				</footer>
			</div>
		</div>
		
		
		
		
		<script type="text/JavaScript" src="./../Resource/jquery/jquery.min.js"></script>
		<script type="text/JavaScript" src="./../Resource/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/JavaScript" src="./../Resource/custom/js/script.js"></script>
	</body>
</html>